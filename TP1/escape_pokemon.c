#include "src/sala.h"
#include "src/objeto.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LENGTH 1024

void consolePrinter(char var[], int flag){
	//Mostrar todos los objetos en la sala

	printf("Objetos...\n");

	//Mostrar si son válidas las siguientes interacciones
	//1. examinar habitacion
	//2. abrir pokebola
	//3. usar llave cajon
	//4. quemar mesa

	printf("Interacciones...\n");
}

char* fileToString(FILE *input){

	char * stringFile = 0;
	long fileLength;

	fseek (input, 0, SEEK_END);
  	fileLength = ftell (input);
  	fseek (input, 0, SEEK_SET);
  	stringFile = malloc (fileLength);

  	if (stringFile)
  	{
    	fread (stringFile, 1, fileLength, input);
  	}
	char *stringFileCopy;
	strncpy(stringFileCopy, stringFile, sizeof(stringFile));
	free(stringFile);
	fclose(input);

	return stringFileCopy;
}

int escapePokemon(FILE *objetos, FILE *interacciones){
	if (objetos == NULL || interacciones == NULL) {
		perror("Error al crear la sala de escape\n");
		return -1;
	}

	sala_t *sala = sala_crear_desde_archivos(fileToString(objetos), fileToString(interacciones));
	return 0;
}

int main(int argc, char *argv[])
{	
	FILE *objetos = fopen(argv[1],"r");
	FILE *interacciones = fopen(argv[2],"r");
	
	escapePokemon(objetos, interacciones);
	
	return 0;
}